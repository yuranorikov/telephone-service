#pragma once

#include "Pair.h"
#include <exception>

template <class K, class T>
/*!
\brief ������ - map (���������� map �� STL)
*/
class Map
{
private:

	Pair<K, T>* items;									///<���� <����, ��������>	- �������� � �������, � ������� �� ���������� map, ��� �������� �������� � ������-������ ������
	unsigned int sz;									///<������� ������

public:
	/*!
	\brief ��������� ����� ���������
	*/
	class iterator;										
	/*!
	\brief ���������� ����� ������������ ���������
	*/
	class const_iterator;								

	Map() : sz(0) { items = new Pair<K, T>[0]; }			///<������ �����������
	~Map() { delete[] items; }								///<����������
	Map(const Map<K, T>& M);								///<����������
	Map(Map<K, T>&& M);										///<������������ ������������

	Map<K, T>& operator= (const Map<K, T>& M);				///<��������� =
	Map<K, T>& operator= (Map<K, T>&& M);
	bool operator != (const Map<K, T>& M) const { return M.items != items; }	///<��������� ���� Map
	bool operator == (const Map<K, T>& M) const { return M.items == items; }    ///<������������ 

	unsigned int size() const { return sz; }				///<��������� ���������� ��������� \return ���-�� ��������� 




	Pair<iterator, bool> insert(Pair<K, T> p)				///<�������, ��������� ���� � ���, �������� ����������� �� ������
	{
		if (find(p.first) != end())							///<���� ����� ���� ��� ����. �� ������������ ���� - ��������, ������������ �� ����� � ���������� false
			return Pair<iterator, bool>(find(p.first), false);
		else
		{													///<���� ������� �� ����������, �� 
			Pair<K, T>* itemsNew = new Pair<K, T>[sz + 1];	///<������������ ������
			int i = 0;
			for (; items[i].first < p.first && sz != i; i++)	///<�������������� ������ �� ����� �������
				itemsNew[i] = items[i];
			iterator it = iterator(*this, i);				///<��������� �������� ��� ��������
			itemsNew[i++] = p;								///<��������� ����
			for (; i <= sz; i++)
				itemsNew[i] = items[i - 1];					///<�������� ������ �����
			delete[] items;									///<������� �������� ������
			items = itemsNew;								///<���������� ���������
			sz++;											///<����������� ������
			return Pair<iterator, bool>(it, true);			///<���������� ���� - ��������� �� ����������� �������, true
		}
	}

	int erase(const iterator& it)							///<��������, ���������� ����� �������� ��������� (� ���� ������������ 0 ��� 1)
	{
		if (*this == it.M && it.cur < sz)					///<���� �������� �������� � ����� ��� � �� ������� �� ������� �������
		{
			Pair<K, T>* itemsNew = new Pair<K, T>[sz - 1];	///<������������ ������
			for (int i = 0; i < it.cur; i++)
				itemsNew[i] = items[i];						///<�������� �� ����� ��������
			for (int i = it.cur; i < sz - 1; i++)
				itemsNew[i] = items[i + 1];					///<����� ����� ��������
			delete[] items;									///<������� �������� ������
			items = itemsNew;								///<�������� ���������
			sz--;
			return 1;//return it;
		}
		return 0;//return end();
	}

	iterator find(K key)									///<����� �� �����, ���������� ��������, ����������� �� ������� � ������ �������� ������
	{
		for (iterator it = begin(); it != end(); it++)
		if (it->first == key)
			return it;
		return end();										///<��� ��������, ����������� �� �����, � ������ ���������� ������
	}

	const_iterator find(K key) const						///<���� �����, ������ ������������ ����������� ��������
	{
		for (const_iterator it = begin(); it != end(); it++)
		if (it->first == key)
			return it;
		return end();
	}

	T& at(K key)											///<������� at, ���������� ������ �� ������ ���� ���� �� �����, ���� ����� ���, �� ���������� ����������
	{
		if (find(key) != end())
			return find(key)->second;
		throw out_of_range("");
	}



	iterator begin() { return iterator(*this, 0); }			///<���������� �������� �� ������
	iterator end() { return iterator(*this, sz); }			///<���������� �������� �� �����

	const_iterator begin() const { return const_iterator(*this, 0); }	///<���������� ����������� ���������
	const_iterator end() const { return const_iterator(*this, sz); }
	/*!
	\brief ����� ��������� ���������� �������� STL, �������� ������� ����������
	*/
	class iterator											
	{
		friend Map;											///<��� � ����������� ��������
		friend const_iterator;
	private:
		Map<K, T>& M;										///<������ �� ���
		unsigned int cur;									///<����� ��������
	public:
		iterator(Map<K, T>& M, unsigned int cur) : M(M), cur(cur) {}	///<����������

		iterator& iterator::operator= (iterator& it)		///< �������� �����
		{
			cur = it.cur;
			M = it.M;
			return *this;
		}

		iterator& operator++()								///<��������� ����������
		{
			++cur;
			return *this;
		}

		iterator operator++(int)							///<�����������
		{
			iterator it(*this);
			++cur;
			return it;
		}

		iterator operator+(int i) {							///< ���� �����
			iterator it(*this);
			it.cur += i;
			return it;
		}

		bool operator!= (const iterator& I) const			///< ��������� ����������
		{
			return M != I.M || cur != I.cur;
		}

		bool operator== (const iterator& I) const			///< ��������� ����������
		{
			return M == I.M && cur == I.cur;
		}


		Pair<K, T>& operator*()								///< �������� �������� ������������� ������ �� ����
		{
			if (cur < 0 || cur >= M.size())
				throw out_of_range("");						///< � ������ ������ �� ������� - ���������� ���������� \throw out_of_range("")
			return M.items[cur];
		}

		Pair<K, T>* operator->()							///< �������� �������� ��������� �� ���� \throw out_of_range("")
		{
			if (cur < 0 || cur >= M.size())
				throw out_of_range("");
			return &M.items[cur];
		}
	};
	/*!
	\brief ����������� ��������, �� ������ ��������� ������� ���
	*/
	class const_iterator									
	{
	private:
		const Map<K, T>& M;									///<����������� ������
		unsigned int cur;									///<�������
	public:
		const_iterator(const Map<K, T>& M, unsigned int cur) : M(M), cur(cur) {}	///<�����������
		const_iterator(const iterator it) : M(it.M), cur(it.cur) {}	///<����������� ���������� � �������� ���������
		const_iterator& operator++()						///<����������
		{
			++cur;
			return *this;
		}

		const_iterator operator++(int)
		{
			const_iterator it(*this);
			++cur;
			return it;
		}

		const_iterator operator+(int i) {
			const_iterator it(*this);
			it.cur += i;
			return it;
		}

		bool operator!= (const const_iterator& I) const		///< ���������
		{
			return M != I.M || cur != I.cur;
		}

		bool operator== (const const_iterator& I) const
		{
			return M == I.M || cur == I.cur;
		}

		const Pair<K, T>& operator*()						///< �������� �������� �������� ������������� ������ �� ���� \throw out_of_range("")
		{
			if (cur < 0 || cur >= M.size())
				throw out_of_range("");
			return M.items[cur];
		}

		Pair<K, T>* operator->()							///< �������� �������� ��������� �� ���� \throw out_of_range("")
		{
			if (cur < 0 || cur >= M.size())
				throw out_of_range("");
			return &M.items[cur];
		}
	};


};

template <class K, class T>
Map<K, T>::Map(const Map<K, T>& M)							///<���������� ����������� ���
{
	items = new Pair<K, T>[M.size()];
	for (sz = 0; sz < M.size(); sz++)
		items[sz] = M.items[sz];
}

template <class K, class T>
Map<K, T>::Map(Map<K, T>&& M)								///<������������ ����������� 
{
	items = M.items;
	sz = M.sz;
	M.items = NULL;
}

template <class K, class T>
Map<K, T>& Map<K, T>::operator= (const Map<K, T>& M)		///<�������� ����������
{
	if (this != &M)
	{
		items = new Pair<K, T>[M.size()];
		for (sz = 0; sz < M.size(); sz++)
			items[sz] = M.items[sz];
	}
	return *this;
}

template <class K, class T>
Map<K, T>& Map<K, T>::operator= (Map<K, T>&& M)				///<�������� ������������
{
	if (this != &M)
	{
		items = M.items;
		sz = M.sz;
		M.items = NULL;
	}
	return *this;
}

