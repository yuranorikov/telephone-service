#include "stdafx.h"
#include "3_3b_12_15_Juridical.h"

#include <sstream>

string Juridical::toString() const
{
	ostringstream ost;
	ost.width(12);
	ost << fullName;
	ost.width(12);
	ost << mail;
	ost.width(12);
	ost << bank;
	ost.width(12);
	ost << type();
	ost.width(12);
	ost << organization;
	return ost.str();
}