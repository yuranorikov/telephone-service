#include "stdafx.h"
#include "3_3b_12_15_Phone.h"

#include <sstream>

string Phone::toString() const
{
	ostringstream ost;
	ost.width(12);
	ost << type();
	ost.width(12);
	ost << number;
	ost.width(12);
	ost << date;
	ost.width(12);
	ost << time;
	ost.width(12);
	ost << duration;
	return ost.str();
}

ostream& operator<< (ostream& s, const Phone& P)
{
	s << P.toString();
	return s;
}