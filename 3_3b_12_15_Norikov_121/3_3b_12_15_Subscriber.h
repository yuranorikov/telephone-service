#pragma once

#include "3_3b_12_15_Individual.h"
#include "3_3b_12_15_Juridical.h"
#include "3_3b_12_15_Phone.h"
#include <vector>
/*!
\brief ��������� ��������
*/
class Subscriber				
{
private:
	Individual* client;			///<��������� �������
	vector<Phone*> services;	///<������ �����, ������������ ������ vector
public:
	
	Subscriber() : client(NULL), services(vector<Phone*>()) {}///<����������� 
	Subscriber(string fullName, string mail, string bank) : client(new Individual(fullName, mail, bank)), services(vector<Phone*>()) {}///<����������� 
	Subscriber(string fullName, string mail, string bank, string organization) : client(new Juridical(fullName, mail, bank, organization)), services(vector<Phone*>()) {}///<����������� 
	Subscriber(Subscriber&&);///<����������� 
	Subscriber(const Subscriber&); ///<����������� 
	~Subscriber() { delete client; } ///<���������� 
	Subscriber& operator= (Subscriber&&); ///<������������ �������� ������������ 
	Subscriber& operator= (const Subscriber&); ///<���������� �������� ������������ 
	friend ostream& operator<< (ostream&, const Subscriber&);   ///<������������� �������� ��� ������
	void add(Phone*);											///<���������� ������
	string getType() const { return client->type(); }			///<��������� ������ - ���� �������  \return ��� �������
	int getDuration(unsigned int i) const;						///<��������� ������������ ������ \param i - ������ \return ������������
	void setDuration(unsigned int i, int duration);				///<��������� ������������ ������ \param i - ������ , duration - ������������ \return ������������
	string getSize(unsigned int services) const;				///<��������� ������ ���������� ������
	int getSizesCount(unsigned int services) const;				///<��������� ����� ����������, ���������� �� �������� ������ � ��������� ������

	void setSize(unsigned int services, double size);			///<��������� ������ ���������� ������ � ����� \param service - ������ ���� , size - ����� \return �����
	void setSizeIn(unsigned int services, double size);			///<��������� ������ ���������� ������ � ���� \param service - ������ ���� , size - ����� \return �����
	void setSizeOut(unsigned int services, double size);
	void setDate(unsigned int services, string date);			///<��������� ���� \param service - ������ ���� , date - ���� \return ����
	void setTime(unsigned int services, string time);			///<��������� ������� \param service - ������ ���� , time - ����� \return �����
	double getTotalSize() const;								///<�������� ����� ����� ���������� ������ ������� ��������
	int getTotalTime(int choice) const;							///<�������� ����� ������ ����� ���������� ���� 
};

