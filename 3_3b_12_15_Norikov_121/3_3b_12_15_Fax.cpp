#include "stdafx.h"
#include "3_3b_12_15_Fax.h"

#include <sstream>

string Fax::toString() const
{
	ostringstream ost;
	ost.width(12);
	ost << type();
	ost.width(12);
	ost << number;
	ost.width(12);
	ost << date;
	ost.width(12);
	ost << time;
	ost.width(12);
	ost << duration;
	ost.width(6);
	ost << size;
	return ost.str();
}
