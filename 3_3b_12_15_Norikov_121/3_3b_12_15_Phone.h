#pragma once

#include <string>

using namespace std;
/*!
\brief ������������ �����
*/
class Phone														///<������� ������ �������
{
protected:
	int number;													///<����� ��������
	string date;												///<����
	string time;												///<�����
	int duration;												///<������������
public:
	Phone() : number(0), date(""), time(""), duration(0) {}
	Phone(int number, string date, string time, int duration) : number(number), date(date), time(time), duration(duration) {}
	virtual ~Phone() {}
	virtual Phone* clone() { return new Phone(*this); }

	int getDuration() const { return duration; }				///<�������� ������������ \return ������������
	void setDuration(int d) { duration = d; }					///<�������� ������������ \return ������������
	void setDate(string d) { date = d; }						///<�������� ���� \return ����
	void setTime(string t) { time = t; }						///<�������� ����� \return �����

	virtual string toString() const;							///<��� ������
	virtual string type() const { return "Phone"; }	            ///<��� ������ \return ��� ���� string
	virtual int getTypeNumber() const { return 0; }				//<��������� ���� ������ \return ��� ����� �������� 0
	virtual double getTotalSize() const { return 0; }			///<����� ���������� ���������� ������ ������ 0 \return 0 �.� ������� ������������ ������ ��������
	friend ostream& operator<< (ostream&, const Phone&);		///<������������� ��������  ��� ������

};

