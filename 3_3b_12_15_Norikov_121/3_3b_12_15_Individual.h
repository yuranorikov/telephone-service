#pragma once

#include <string>

using namespace std;
/*!
\brief ������������ ����� ��� �������������� ����
*/
class Individual									///<���������� ����
{
protected:
	string fullName;								///<���
	string mail;									///<�����
	string bank;									///<���������� �����

public:
	//������������
	Individual() : fullName(""), mail(""), bank(0) {}
	Individual(string fullName, string mail, string bank) : fullName(fullName), mail(mail), bank(bank) {}
	virtual ~Individual() {}
	virtual Individual* clone() { return new Individual(*this); }

	virtual string toString() const;							///<��� ������
	virtual string type() const { return "Individual"; }		///<��� ������� \return �������� ����
	friend ostream& operator<< (ostream&, const Individual&);	///<������������� ��������  ��� ������

};

