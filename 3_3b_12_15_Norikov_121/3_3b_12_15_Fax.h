#pragma once
#include "3_3b_12_15_Phone.h"
/*!
\brief �������� ����� 
������� �������� �����, ������� ������������ �� ����� ���������� ������ Phone
*/

class Fax :
	public Phone
{
private:
	double size;											///<���-�� ���������� ������, ��������� ����������� �� Phone
public:
	Fax() : Phone() { size = 0; }
	Fax(int number, string date, string time, int duration, double s) : Phone(number, date, time, duration) { size = s; }
	virtual ~Fax() {}
	virtual Phone* clone() { return new Fax(*this); }
		
	double getSize() const { return size; }					///<��������� ���-�� ���������� ������ \return ���-�� ���������� ������
	void setSize(double s) { size = s; }					///<��������� ���-�� ���������� ������ \return ����� ���-�� ���������� ������ s

	virtual string toString() const;						///<��� ������ 
	virtual string type() const { return "Fax"; }			///<��������� ���� \return �������� ����
	virtual int getTypeNumber() const { return 1; }			///<��������� ���� \return ����� ���� ������
	virtual double getTotalSize() const { return size; }	///<��������� ������ ���������� ������ \return ���-�� ���������� ������
};

