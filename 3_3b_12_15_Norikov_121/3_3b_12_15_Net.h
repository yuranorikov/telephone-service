#pragma once
#include "3_3b_12_15_Phone.h"
/*!
\brief �������� ����� 
������� �������� �����, ������� ������������ �� ����� ���������� ������ Phone
*/
class Net :
	public Phone														///<������ ���� ����������� �� ���������� �����
{
private:
	double sizeIn;														///<����� ��������� ��������
	double sizeOut;														///<����� ���������� ��������
public:
	Net() : Phone() { sizeIn = sizeOut = 0; }
	Net(int number, string date, string time, int duration, double sIn, double sOut) : Phone(number, date, time, duration) { sizeIn = sIn; sizeOut = sOut; }
	virtual ~Net() {}
	virtual Phone* clone() { return new Net(*this); }

	double getSizeIn() const { return sizeIn; }							///<��������� ������ �������
	double getSizeOut() const { return sizeOut; }
	void setSizeIn(double size) { sizeIn = size; }
	void setSizeOut(double size) { sizeOut = size; }

	virtual string toString() const;
	virtual string type() const { return "Net"; }						///<��� ������� \return �������� ����
	virtual int getTypeNumber() const { return 2; }
	virtual double getTotalSize() const { return sizeIn + sizeOut; }	///<����� ����� ���������� ����� - ����� ������� � ��������� \return ����� ����� ���������� ����� - ����� ������� � ���������
};

