#pragma once

#include "3_3b_12_15_Individual.h"
#include <string>

using namespace std;
/*!
\brief �������� �����
������� �������� �����, ������� ������������ �� ����� ���������� ������ Individual
*/
class Juridical :									///<����������� ����
	public Individual                               ///<������������ �� ������������� ������
{
private:
	string organization;							///<�������������� ���� - ��� �����������

public:
	Juridical() : Individual() {}
	Juridical(string fullName, string mail, string bank, string o) : Individual(fullName, mail, bank) { organization = o; }
	virtual ~Juridical() {}
	virtual Individual* clone() { return new Juridical(*this); }

	virtual string toString() const;				///<�����
	virtual string type() const { return "Juridical"; }	///<��� ������� \return �������� ����
};

