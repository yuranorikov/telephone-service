#include "stdafx.h"
#include "3_3b_12_15_Subscriber.h"
#include "3_3b_12_15_Fax.h"
#include "3_3b_12_15_Net.h"

#include <sstream>


Subscriber::Subscriber(Subscriber&& S)
{
	client = S.client;
	services = S.services;
	S.client = NULL;
	S.services = vector<Phone*>();
}

Subscriber::Subscriber(const Subscriber& S)
{
	client = S.client->clone();
	services = vector<Phone*>();
	for (vector<Phone*>::const_iterator it = S.services.begin(); it != S.services.end(); it++)
		services.push_back((*it)->clone());
}

Subscriber& Subscriber::operator= (Subscriber&& S)
{
	if (this != &S)
	{
		client = S.client;
		services = S.services;
		S.client = NULL;
		S.services = vector<Phone*>();
	}
	return *this;
}
Subscriber& Subscriber::operator= (const Subscriber& S)
{
	if (this != &S)
	{
		client = S.client->clone();
		services = vector<Phone*>();
		for (vector<Phone*>::const_iterator it = S.services.begin(); it != S.services.end(); it++)
			services.push_back((*it)->clone());
	}
	return *this;
}

ostream& operator<< (ostream& s, const Subscriber& S)
{
	s << *S.client << endl;
	if (S.services.size())
	{
		s << "          services:" << endl;
		s.width(12);
		s << "type";
		s.width(12);
		s << "number";
		s.width(12);
		s << "date";
		s.width(12);
		s << "time";
		s.width(12);
		s << "duration";
		s.width(12);
		s << "size" << endl;
	}
	for each (Phone* p in S.services)
		s << *p << endl;
	return s;
}

void Subscriber::add(Phone* P)
{
	services.push_back(P);
}

int Subscriber::getDuration(unsigned int i) const
{
	if (i >= services.size())
		throw exception("Incorrect service");
	else
		return services[i]->getDuration();
}

void Subscriber::setDuration(unsigned int i, int duration)
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		services[i]->setDuration(duration);
}

string Subscriber::getSize(unsigned int i) const
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		if (typeid(*services[i]) == typeid(Phone))
			return "No sizes";
		else if (typeid(*services[i]) == typeid(Fax))
		{
			ostringstream ost;
			ost << "Size = " << ((Fax*)(services[i]))->getSize();
			return ost.str();
		}
		else
		{
			ostringstream ost;
			ost << "Size in = " << ((Net*)(services[i]))->getSizeIn() << " size out = " << ((Net*)(services[i]))->getSizeOut();
			return ost.str();
		}
}

int Subscriber::getSizesCount(unsigned int i) const
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		return services[i]->getTypeNumber();
}

void Subscriber::setSize(unsigned int i, double size)
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		if (typeid(*services[i]) == typeid(Fax))
			((Fax*)(services[i]))->setSize(size);			
}

void Subscriber::setSizeIn(unsigned int i, double size)
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		if (typeid(*services[i]) == typeid(Net))
			((Net*)(services[i]))->setSizeIn(size);
}

void Subscriber::setSizeOut(unsigned int i, double size)
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		if (typeid(*services[i]) == typeid(Net))
			((Net*)(services[i]))->setSizeOut(size);
}


void Subscriber::setDate(unsigned int i, string date)
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		services[i]->setDate(date);
}
void Subscriber::setTime(unsigned int i, string time)
{
	if (i >= services.size() || i < 0)
		throw exception("Incorrect service");
	else
		services[i]->setTime(time);

}

double Subscriber::getTotalSize() const
{
	double totalSize = 0;
	for (unsigned int i = 0; i < services.size(); i++)
		totalSize += services[i]->getTotalSize();
	return totalSize;
}

int Subscriber::getTotalTime(int choice) const
{
	int totalTime = 0;
	for (unsigned int i = 0; i < services.size(); i++)
		if (services[i]->getTypeNumber() == choice)
			totalTime += services[i]->getDuration();
	return totalTime;
}