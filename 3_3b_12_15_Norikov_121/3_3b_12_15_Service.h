#pragma once

#include <string>
#include <iostream>
#include <exception>
#include <map>
#include "3_3b_12_15_Subscriber.h"
#include "Map.h"
#define map Map
#define pair Pair
/*! \mainpage My Personal Index Page
*
* \section intro_sec Introduction
*
* the program is a set of classes for working with Telephone Service
*
* describes the classes to work with three types of users:
*
* Individua and Juridical 
*
*information on all users is stored in the Table
*
*Table is stored in the main class of the service 
*
*may add a new user in the table
*
* \may change information about the users
*
*\may get information about users:
*
*\add informations and servises for users
*
*
*
*
* create by Yuriy Norikov
* MEPHI k03-121
*/
/*!
\file
\brief ������������ ���� � ��������� �������

������ ���� �������� � ���� ����������� ��������
�������, ������������ � ���������������� ���������
*/

using namespace std;
/*!
\brief ��������� ��������
*/
class Service					///<������
{
private:
	string name;				///<��� �������
	int payment;				///<�������� �����
	int communicationRate;		///<����� ������ ������ �����
	int trafficRate;			///<����� ������ ������ ��
	map<int,Subscriber*> table;	///<������� ���������, ������������ ������ map
public:
	//������������
	Service() :name(""), payment(0), communicationRate(0), trafficRate(0), table(map<int, Subscriber*>()) {}///<�����������
	Service(string n) :Service() { name = n; }///<�����������
	Service(Service&&);///<�����������
	Service(const Service&);///<�����������
	 ~Service() ;///<����������
	
	Service& operator= (Service&&);///<������������ �������� ������������ 
	Service& operator= (const Service&);///<���������� �������� ������������
	friend ostream& operator<< (ostream&, const Service&);///<������������� ��������  ��� ������

	int getPayment() const { return payment; }						///<��������� ������� ����������� ����� \return ������� ����������� ����� 
	int getCommunicationRate() const { return communicationRate; }	///<��������� �������� ������ c���� \return �������� ������ c����
	int getTrafficRate() const { return trafficRate; }				///<��������� �������� ������ ��������� \return �������� ������� 
	string getType(int client) const;								///<������ - ��������� ��������
	int getDuration(int client, int services) const;				///<��������� ������������ ����������� ������ � ������������ �������� \return ������������
	void setDuration(int client, int services, int duration);		///<��������� ������������ ����������� ������ � ������������ �������� \return ������������
	string getSize(int client, int services) const;					///<��������� ������ ����������, ���������� ����������� ������� � ������������ �������� \return ����� ����������
	int getSizesCount(int client, int services) const;				///<�������� ����� ����������, ���������� �� �������� ������ \return ����� ����������
	void setSize(int client, int services, double size);			///<�������� ������ ���������� ������ �� ����� \return ����� ���������� ������
	void setSizeIn(int client, int services, double size);			///<�������� ������ ���������� ������ �� ���� (��������) \return ����� ���������� ������
	void setSizeOut(int client, int services, double size);			///<�������� ������ ���������� ������ �� ���� (���������) \return ����� ���������� ������
	void setDate(int client, int services, string date);			///<�������� ���� \return ����
	void setTime(int client, int services, string time);			///<�������� ����� \return �����
	double getTotalSize(int client) const;							///<�������� ����� ���-�� ���������� ���������� �� ������� ������� \return ����� ���-�� ���������� ���������� �� ������� �������
	int getTotalTime(int choice) const;								///<��������� ������ ������� ����������� ������������ ���� ������� \return ����� ����� ����������� ����������� �������

	Subscriber* find(int number) const;								///<����� ��������� �������� �� ������ \return ��������� ��������
	void add(int number, string fullName, string mail, string bank, string organization);	///<�������� ��. ����
	void add(int number, string fullName, string mail, string bank);						///<�������� ���.����
	void addService(int client, int number, string date, string time, int duration);		///<�������� ������ - �������
	void addService(int client, int number, string date, string time, int duration, double size);	///<�������� ������ - ����
	void addService(int client, int number, string date, string time, int duration, double sizeIn, double sizeOut);	///<�������� ������ - ����

};

