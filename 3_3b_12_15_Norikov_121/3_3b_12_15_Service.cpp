
#include "stdafx.h"
#include "3_3b_12_15_Service.h"
#include "3_3b_12_15_Fax.h"
#include "3_3b_12_15_Net.h"


Service::Service(Service&& S)
{
	name = S.name;
	payment = S.payment;
	communicationRate = S.communicationRate;
	trafficRate = S.trafficRate;
	table = S.table;
	S.table = map<int, Subscriber*>();
}

Service::Service(const Service& S)
{
	name = S.name;
	payment = S.payment;
	communicationRate = S.communicationRate;
	trafficRate = S.trafficRate;
	table = map<int,Subscriber*>();
	for (map<int, Subscriber*>::iterator it = table.begin(); it != table.end(); it++)
		table.insert(pair<int, Subscriber*>(it->first, new Subscriber(*it->second)));
}

Service::~Service()
{
	for (map<int, Subscriber*>::iterator it = table.begin(); it != table.end(); it++)
		delete it->second;
}

Service& Service::operator= (Service&& S)
{
	if (this != &S)
		table = S.table;
	return *this;
}

Service& Service::operator= (const Service& S)
{
	if (this != &S)
		table = map<int, Subscriber*>();
	for (map<int, Subscriber*>::iterator it = table.begin(); it != table.end(); it++)
		table.insert(pair<int, Subscriber*>(it->first, new Subscriber(*it->second)));
	return *this;
}

ostream& operator<< (ostream& s, const Service& S)
{
	s << "\"" << S.name << "\"" << endl;
	s.width(12);
	s << "number";
	s.width(12);
	s << "fullname";
	s.width(12);
	s << "mail";
	s.width(12);
	s << "bank";
	s.width(12);
	s << "type";
	s.width(12);
	s << " organization" << endl;

	for each(pair<int, Subscriber*> p in S.table)
	{
		s.width(12);
		s << p.first << *p.second << endl;
	}
	return s;
}

Subscriber* Service::find(int number) const
{
	map<int, Subscriber*>::const_iterator it = table.find(number);
	if (it != table.end())
		return it->second;
	return NULL;
}

void Service::add(int number, string fullName, string mail, string bank)
{
	if (!table.insert(pair<int, Subscriber*>(number, new Subscriber(fullName, mail, bank))).second)
		throw exception("Product exist");
}

void Service::add(int number, string fullName, string mail, string bank, string organization)
{
	if (!table.insert(pair<int, Subscriber*>(number, new Subscriber(fullName, mail, bank, organization))).second)
		throw exception("Product exist");
}

void Service::addService(int client, int number, string date, string time, int duration)
{
	if (find(client))
		find(client)->add(new Phone(number, date, time, duration));
	else
		throw exception("Subscriber not found");
}

void Service::addService(int client, int number, string date, string time, int duration, double size)
{
	if (find(client))
		find(client)->add(new Fax(number, date, time, duration, size));
	else
		throw exception("Subscriber not found");
}

void Service::addService(int client, int number, string date, string time, int duration, double sizeIn, double sizeOut)
{
	if (find(client))
		find(client)->add(new Net(number, date, time, duration, sizeIn, sizeOut));
	else
		throw exception("Subscriber not found");
}

string Service::getType(int client) const
{
	if (find(client))
		return find(client)->getType();
	else
		throw exception("Subscriber not found");

}

int Service::getDuration(int client, int services) const
{
	if (find(client))
		return find(client)->getDuration(services);
	else
		throw exception("Subscriber not found");
}


void Service::setDuration(int client, int services, int duration)
{
	if (find(client))
		find(client)->setDuration(services, duration);
	else
		throw exception("Subscriber not found");
}

string Service::getSize(int client, int services) const
{
	if (find(client))
		return find(client)->getSize(services);
	else
		throw exception("Subscriber not found");
}


int Service::getSizesCount(int client, int services) const
{
	if (find(client))
		return find(client)->getSizesCount(services);
	else
		throw exception("Subscriber not found");
}

void Service::setSize(int client, int services, double size)
{
	if (find(client))
		return find(client)->setSize(services, size);
	else
		throw exception("Subscriber not found");
}

void Service::setSizeIn(int client, int services, double size)
{
	if (find(client))
		return find(client)->setSizeIn(services, size);
	else
		throw exception("Subscriber not found");
}

void Service::setSizeOut(int client, int services, double size)
{
	if (find(client))
		return find(client)->setSizeOut(services, size);
	else
		throw exception("Subscriber not found");
}

void Service::setDate(int client, int services, string date)
{
	if (find(client))
		return find(client)->setDate(services, date);
	else
		throw exception("Subscriber not found");
}

void Service::setTime(int client, int services, string time)
{
	if (find(client))
		return find(client)->setTime(services, time);
	else
		throw exception("Subscriber not found");
}

double Service::getTotalSize(int client) const
{
	if (find(client))
		return find(client)->getTotalSize();
	else
		throw exception("Subscriber not found");
}

int Service::getTotalTime(int choice) const
{
	int time = 0;
	for (map<int, Subscriber*>::const_iterator it = table.begin(); it != table.end(); it++)
		time += it->second->getTotalTime(choice);
	return time;
}