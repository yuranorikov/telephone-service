#pragma once

template <class K, class T>
/*!
\brief ������ ������ ����, �������� ������ ��� ���� � �����������, ���������� ������� STL: pair
*/
class Pair							
{
public:
	K first;  ///< \param K ����
	T second; ///< \param T ��������
public:
	Pair() :first(K()), second(T()) {  } ///< �����������
	Pair(K arg1, T arg2) : first(arg1), second(arg2) { }///< �����������
	~Pair() {} ///< ���������� 
};

