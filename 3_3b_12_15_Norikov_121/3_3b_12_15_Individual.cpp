#include "stdafx.h"
#include "3_3b_12_15_Individual.h"
#include <sstream>


string Individual::toString() const
{
	ostringstream ost;
	ost.width(12);
	ost << fullName;
	ost.width(12);
	ost << mail;
	ost.width(12);
	ost << bank;
	ost.width(12);
	ost << type();
	return ost.str();
}

ostream& operator<< (ostream& s, const Individual& I)
{
	s << I.toString();
	return s;
}

