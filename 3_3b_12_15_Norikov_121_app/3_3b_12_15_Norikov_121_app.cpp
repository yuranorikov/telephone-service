// 3_3_12_15_Norikov_121_app.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Service.h"

#include <iostream>
#include <string>
#include <exception>

using namespace std;


string msgs[] = { "Exit", "Print menu", "Print service", "Add subscriber", "Add service", "Get subscriber type",
"Get duration of service", "Set duration of service", "Get size of information", "Set size of information",
"Set date", "Set time", "Get total subsriber size", "Get total time for service" };
const int amount = 14;

Service* CreateNew();						//�������� ������ �������

int Exit(Service*);							//������� ������
int Menu(Service*);							//����
int Print(Service*);						//����� �������
int AddClient(Service*);					//�������� ��������
int AddService(Service*);					//�������� ������ ��������
int GetType(Service*);						//��������� ���� ��������
int GetDuration(Service*);					//��������� ������������ �������� ������
int SetDuration(Service*);					//��������� ������������ �������� ������
int GetSize(Service*);						//��������� ������ ���������� ������
int SetSize(Service*);						//���������� ������ ��������� ������
int SetDate(Service*);						//�������� ����
int SetTime(Service*);						//��������� �����
int GetTotal(Service*);						//��������� ����� ����� ������������ ���������� ��� ��������
int GetTotalTime(Service*);					//��������� ����� ����� ������������� �������� �� ��� ����

int getInt();								//������� ���������� ������ �����
double getDouble();

int main()
{

	Service* S;
	try
	{
		S = CreateNew();
	}
	catch (bad_alloc)												//�������� ����������, ���������� � ��������� ������
	{
		cout << "Out of memory. :-(" << endl;
		return 1;
	}
	int ex = 0;														//��������� ��� �������� ������ ����
	char choice = 0;												//����� �� ����� - ����� ����
	for (int i = 0; i<amount; ++i)
	{
		cout << (char)(i + 'a') << " " << msgs[i] << ",";
	}
	int(*func[amount]) (Service*) = { Exit, Menu, Print, AddClient, AddService, GetType, GetDuration, SetDuration, GetSize, SetSize, SetDate, SetTime, GetTotal, GetTotalTime };					//������ �������

	while (!ex)
	{																//���� ������ ������
		cout << "\nMake your choice: ";
		cin >> choice;
		while (!((choice>('a' - 1)) && ((choice<amount + 'a'))))	//���� �� ����� ���������� ����� ������
			cin >> choice;
		if ((choice>('a' - 1)) && ((choice<amount + 'a')))
			try
		{
			ex = (*func[choice - 'a'])(S);							//������� ��������, ������� ������� ��������
		}
		catch (bad_alloc)
		{
			ex = 1;
			cout << "Out of memory!" << endl;
		}
	}

	return 0;
}

//������� �������� ������ �������
Service* CreateNew()
{
	string name = "";
	cout << "Enter name of service" << endl;
	cin >> name;
	Service* S = new Service(name);
	return S;
}

int Exit(Service* S) 													//���������� ���������
{
	delete S;															//������� ������
	return 1;
}

int Menu(Service*)														//����� ����
{
	int i;
	printf("\n");
	for (i = 0; i < amount; ++i)
		cout << (char)(i + 'a') << " " << msgs[i] << ", ";
	return 0;
}

int Print(Service* S)
{
	cout << "Service: " << endl << *S;									//�����
	return 0;
}

int AddClient(Service* S)
{
	cout << "Enter subscriber's number" << endl;						//����� �������
	int number = getInt();
	cout << "Enter subscriber's mail" << endl;
	string mail = "";													//����� �������
	cin >> mail;
	cout << "Enter bank number" << endl;
	string bank = "";													//���������� ����� (������ ������)
	cin >> bank;
	cout << "Enter full Name" << endl;
	string fullName = "";												//���
	cin >> fullName;
	cout << "Enter type of client: 'j' - juridical, other - individual" << endl;
	char c = 'j';
	cin >> c;

	try
	{
		if (c == 'j')
		{
			cout << "Enter organization name" << endl;
			string organization = "";									//��� ����������� ��� ����������� ���
			cin >> organization;
			S->add(number, fullName, mail, bank, organization);
		}
		else
			S->add(number, fullName, mail, bank);
		cout << "Client added." << endl;
	}
	catch (bad_alloc)
	{
		cout << "Out of memory." << endl;
		return 1;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int AddService(Service* S)
{
	cout << "Enter number of subscriber" << endl;									//�������� ����� ������� ��������
	int client = getInt();
	if (!S->find(client))
	{
		cout << "Subscriber not found" << endl;										//����� ��������
		return 0;
	}
	cout << "Enter type of service: 'n' - net, 'f' - fax, other - individual" << endl;	//�������� ��� ������
	char c = 'o';
	cin >> c;
	cout << "Enter partner's number" << endl;										//����� ��������
	int number = getInt();
	cout << "Enter date" << endl;													//���� (������ ������)
	string date = "";
	cin >> date;
	cout << "Enter time" << endl;													//����� (������ ������)
	string time = "";
	cin >> time;
	cout << "Enter duration" << endl;												//������������
	int duration = getInt();
	try
	{
		if (c == 'f')
		{
			cout << "Enter size of information (MB)" << endl;						//����� ����� ���������� ��� �����
			double size = getDouble();
			S->addService(client, number, date, time, duration, size);
		}
		else if (c == 'n')															//����� ���� ��� ����
		{
			cout << "Enter size of incoming information (MB)" << endl;
			double sizeIn = getInt();
			cout << "Enter size of outgoing information (MB)" << endl;
			double sizeOut = getDouble();
			S->addService(client, number, date, time, duration, sizeIn, sizeOut);
		}
		else
			S->addService(client, number, date, time, duration);
		cout << "Service added." << endl;
	}
	catch (bad_alloc)
	{
		cout << "Out of memory." << endl;
		return 1;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int GetType(Service* S)
{
	cout << "Enter number of subscriber" << endl;								//��������� ���� ��������
	int client = getInt();
	try
	{
		cout << "Type: " << S->getType(client) << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int GetDuration(Service* S)
{
	cout << "Enter number of subscriber" << endl;								//������������ ����������� ������
	int client = getInt();
	cout << "Enter number of services" << endl;
	int services = getInt();
	try
	{
		cout << "Duration: " << S->getDuration(client, services) << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int SetDuration(Service* S)
{
	cout << "Enter number of subscriber" << endl;
	int client = getInt();
	cout << "Enter number of services" << endl;
	int services = getInt();
	cout << "Enter new duration" << endl;
	int duration = getInt();
	try
	{
		S->setDuration(client, services, duration);								//��������� ������������ ������������
		cout << "Duration changed " << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int GetSize(Service* S)
{
	cout << "Enter number of subscriber" << endl;
	int client = getInt();
	cout << "Enter number of services" << endl;
	int services = getInt();
	try
	{
		cout << S->getSize(client, services) << endl;							//������� ����� ������, ����������� �������
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int SetSize(Service* S)
{
	cout << "Enter number of subscriber" << endl;								//�������� ����� ������
	int client = getInt();
	cout << "Enter number of services" << endl;
	int services = getInt();
	int sizes = S->getSizesCount(client, services);
	try
	{
		if (sizes == 0)															//��������� ������� ���������� ����� ����� �������� (0, 1 ��� 2)
			cout << "It is a Phone!" << endl;
		else if (sizes == 1)
		{
			cout << "Enter size of information (MB)" << endl;
			double size = getDouble();
			S->setSize(client, services, size);
		}
		else
		{
			cout << "Enter size of incoming information (MB)" << endl;
			double sizeIn = getInt();
			S->setSizeIn(client, services, sizeIn);
			cout << "Enter size of outgoing information (MB)" << endl;
			double sizeOut = getDouble();
			S->setSizeOut(client, services, sizeOut);
		}
		cout << "Sizes changed" << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int SetDate(Service* S)
{
	cout << "Enter number of subscriber" << endl;
	int client = getInt();
	cout << "Enter number of services" << endl;
	int services = getInt();
	cout << "Enter date" << endl;
	string date = "";
	cin >> date;
	try
	{
		S->setDate(client, services, date);									//�������� ���� ������
		cout << "Date changed " << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int SetTime(Service* S)
{
	cout << "Enter number of subscriber" << endl;
	int client = getInt();
	cout << "Enter number of services" << endl;
	int services = getInt();
	cout << "Enter time" << endl;
	string time = "";
	cin >> time;
	try
	{
		S->setTime(client, services, time);										//�������� ����� ������
		cout << "Time changed " << endl;
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int GetTotal(Service* S)
{
	cout << "Enter number of subscriber" << endl;
	int client = getInt();
	try
	{
		cout << "Total =  " << S->getTotalSize(client) << endl;					//�������� ����� ����� �������
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int GetTotalTime(Service* S)
{
	cout << "Enter type of service ('0' - Phone, '1' - Fax, '2' - Net)" << endl;
	char choice = 'f';
	cin >> choice;
	try
	{
		cout << "Total time of service =  " << S->getTotalTime(choice - '0') << endl;	//�������� ����� �����
	}
	catch (exception e)
	{
		cout << e.what();
	}
	return 0;
}

int getInt()														//��������� ������ �����, 
{
	int n = 0;
	do																//���� �� ����� ������ ����������
	{
		cin.clear();												//������� ������ 
		cin.ignore();												//������������� ������
		cin >> n;
	} while (!cin.good());											//���� ����� �� good(), �� ���������� ��������, ������� ����� �����, ��������������� �����
	return n;
}

double getDouble()													//��������� �������� �����, 
{
	double n = 0;
	do																//���� �� ����� ������ ����������
	{
		cin.clear();												//������� ������ 
		cin.ignore();												//������������� ������
		cin >> n;
	} while (!cin.good());											//���� ����� �� good(), �� ���������� ��������, ������� ����� �����, ��������������� �����
	return n;
}


