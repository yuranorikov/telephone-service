// 3_3_12_15_Norikov_121_test.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "gtest\gtest.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Phone.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Net.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Fax.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Individual.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Juridical.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Subscriber.h"
#include "..\3_3b_12_15_Norikov_121\3_3b_12_15_Service.h"

TEST(Testing, CheckPhone)
{
	Phone P = Phone(1, "30.10", "32:10", 5);
	ASSERT_EQ(P.getDuration(), 5);
	ASSERT_EQ(P.getTotalSize(), 0);
	ASSERT_EQ(P.getTypeNumber(), 0);
	P.setDuration(6);
	ASSERT_EQ(P.getDuration(), 6);
	ASSERT_STREQ(P.toString().c_str(), "       Phone           1       30.10       32:10           6");
	ASSERT_STREQ(P.type().c_str(), "Phone");
}

TEST(Testing, CheckNet)
{
	Net N = Net(1, "30.10", "32:10", 5, 100, 200);
	ASSERT_EQ(N.getDuration(), 5);
	ASSERT_EQ(N.getSizeIn(), 100);
	ASSERT_EQ(N.getSizeOut(), 200);
	ASSERT_EQ(N.getTotalSize(), 300);
	N.setDuration(6);
	ASSERT_EQ(N.getDuration(), 6);
	N.setSizeIn(250);
	ASSERT_EQ(N.getSizeIn(), 250);
	N.setSizeOut(320);
	ASSERT_EQ(N.getSizeOut(), 320);
	ASSERT_EQ(N.getTotalSize(), 570);
	ASSERT_STREQ(N.toString().c_str(), "         Net           1       30.10       32:10           6   250   320");
	ASSERT_STREQ(N.type().c_str(), "Net");
}

TEST(Testing, CheckFax)
{
	Fax F = Fax(1, "30.10", "32:10", 5, 100);
	ASSERT_EQ(F.getDuration(), 5);
	ASSERT_EQ(F.getSize(), 100);
	ASSERT_EQ(F.getTotalSize(), 100);
	F.setDuration(6);
	ASSERT_EQ(F.getDuration(), 6);
	F.setSize(120);
	ASSERT_EQ(F.getSize(), 120);
	F.setSize(120);
	ASSERT_EQ(F.getTotalSize(), 120);
	ASSERT_STREQ(F.toString().c_str(), "         Fax           1       30.10       32:10           6   120");
	ASSERT_STREQ(F.type().c_str(), "Fax");
}

TEST(Testing, CheckIndividual)
{
	Individual I = Individual("Ann C", "anna@w.com", "ipb");
	ASSERT_STREQ(I.type().c_str(), "Individual");
	ASSERT_STREQ(I.toString().c_str(), "       Ann C  anna@w.com         ipb  Individual");
}

TEST(Testing, CheckJuridical)
{
	Juridical J = Juridical("Ann C", "anna@w.com", "ipb", "MEPhI");
	ASSERT_STREQ(J.type().c_str(), "Juridical");
	ASSERT_STREQ(J.toString().c_str(), "       Ann C  anna@w.com         ipb   Juridical       MEPhI");
}

TEST(Testing, CheckSubscriber)
{
	Subscriber S1 = Subscriber("Ann C", "anna@w.com", "ipb");
	S1.add(new Phone(1, "30.10", "32:10", 5));
	S1.add(new Fax(2, "29.10", "22:10", 4, 100));
	S1.add(new Net(3, "30.10", "32:10", 3, 300, 200));
	ASSERT_EQ(S1.getDuration(2), 3);
	ASSERT_ANY_THROW(S1.getDuration(3));
	ASSERT_STREQ(S1.getSize(0).c_str(), "No sizes");
	ASSERT_STREQ(S1.getSize(1).c_str(), "Size = 100");
	ASSERT_STREQ(S1.getSize(2).c_str(), "Size in = 300 size out = 200");		//�������� ��������� �������� �����
	ASSERT_ANY_THROW(S1.getSize(-1));
	ASSERT_EQ(S1.getSizesCount(0), 0);
	ASSERT_EQ(S1.getSizesCount(1), 1);
	ASSERT_EQ(S1.getSizesCount(2), 2);
	ASSERT_ANY_THROW(S1.getSizesCount(-1));										//�������� ���������� �� ������ ������������ ��������
	ASSERT_EQ(S1.getTotalSize(), 600);
	ASSERT_EQ(S1.getTotalTime(0), 5);
	ASSERT_STREQ(S1.getType().c_str(), "Individual");
	S1.setSize(1, 10);
	ASSERT_STREQ(S1.getSize(1).c_str(), "Size = 10");
	Subscriber S2 = Subscriber("Ann C", "anna@w.com", "ipb", "MS");
	ASSERT_STREQ(S2.getType().c_str(), "Juridical");
	Subscriber S5 = Subscriber(S2);
	ASSERT_STREQ(S5.getType().c_str(), "Juridical");
	S2 = S1;																	//�������� ��������� ������������
	ASSERT_STREQ(S2.getSize(0).c_str(), "No sizes");
	ASSERT_STREQ(S2.getSize(1).c_str(), "Size = 10");
	ASSERT_STREQ(S2.getSize(2).c_str(), "Size in = 300 size out = 200");
	Subscriber S3 = Subscriber(S2);												//�������� ������������
	ASSERT_STREQ(S3.getSize(0).c_str(), "No sizes");
	ASSERT_STREQ(S3.getSize(1).c_str(), "Size = 10");
	ASSERT_STREQ(S3.getSize(2).c_str(), "Size in = 300 size out = 200");
	Subscriber S4 = Subscriber(S2);

}

TEST(Testing, CheckService)
{
	Service S = Service("service");
	S.add(101, "Ann C", "anna@w.com", "ipb", "MEPhI");
	S.add(11, "Ann R", "anna@w.com", "ipb");
	S.addService(101, 1, "30.10", "32:10", 5);
	S.addService(101, 2, "29.10", "22:10", 4, 100);
	S.addService(101, 3, "30.10", "32:10", 3, 300, 200);
	S.addService(11, 1, "30.10", "32:10", 5);
	S.addService(11, 2, "29.10", "22:10", 4, 100);
	S.addService(11, 3, "30.10", "32:10", 3, 300, 200);
	ASSERT_ANY_THROW(S.addService(10, 0, "", "", 2));
	ASSERT_EQ(S.find(101)->getDuration(0), 5);
	ASSERT_EQ(S.getTotalSize(101), 600);
	ASSERT_EQ(S.getTotalTime(0), 10);
}


int _tmain(int argc, _TCHAR* argv[])
{
	int x;
	::testing::InitGoogleTest(&argc, argv);
	x = RUN_ALL_TESTS();
	system("pause");
	return x;
}