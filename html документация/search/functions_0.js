var searchData=
[
  ['add',['add',['../class_service.html#a8f6c4dd575cb8a302688e46646eb063c',1,'Service::add(int number, string fullName, string mail, string bank, string organization)'],['../class_service.html#afdf0f2f7b3f4c301a2d06c5288f34d89',1,'Service::add(int number, string fullName, string mail, string bank)'],['../class_subscriber.html#a7e0b0451d4bb0e7f9240e6bc5d667907',1,'Subscriber::add()']]],
  ['addservice',['addService',['../class_service.html#a18387ebbe0f8fa8ee3b764e05a1a841b',1,'Service::addService(int client, int number, string date, string time, int duration)'],['../class_service.html#af514fa02ef45c137af319ef597bd797f',1,'Service::addService(int client, int number, string date, string time, int duration, double size)'],['../class_service.html#a7dc96ddede866a8afa4b77e802abe3c6',1,'Service::addService(int client, int number, string date, string time, int duration, double sizeIn, double sizeOut)']]],
  ['at',['at',['../class_map.html#a68f72208cdb1bd6f7c09cffe6679ae96',1,'Map']]]
];
